title: Virtual Workshop
disable_toc: true
---

![Workshop poster](/images/workshop.png)

## Introduction

Learn how to use CinCan tools at our hands-on workshop!

CinCan is a project helping analysts install and run their [DFIR tools](/tools). In this
workshop we will:

* Demostrate running containerized tools with our automation tool [`cincan-command`](https://gitlab.com/cincan/cincan-command).
* Let you perform DFIR exercises with chat support (4 different task in 4 hours)
* Wrap up the session with solution speedruns by CinCan project staff

### General info

* Thursday, 11 June 2020
* [8:00 - 12:00 (UTC), 11:00 - 15:00 in Finnish time (.ics)](workshop.ics)
* Start at **8:00** in the [main Whereby Meeting room](https://cincan.whereby.com/workshop)
* The tasks at [CinCan GitLab](https://gitlab.com/CinCan/workshops/-/tree/master/Virtual%20Workshop)
* Fill a [short survey](https://docs.google.com/forms/d/e/1FAIpQLScBBE4D8aIUSCeOd_cApN8WlDguJh45dKGeY_hNhrRZdqIg3g/viewform?usp=sf_link) ideally before leaving the workshop

The workshop will take place in Whereby meeting rooms. We will have different
meeting rooms based on the task. Our project members will answer your
questions and guide you through the analysis. In each room, different analysis
tools are used, but our `cincan` command line tool will act as an inteface to
all of them.

After the workshop, you can fill a short survey about the tasks where you also have the opportunity to provide us feedback about the CinCan project. Participate in the survey here: [CinCan Virtual Workshop Survey](https://docs.google.com/forms/d/e/1FAIpQLScBBE4D8aIUSCeOd_cApN8WlDguJh45dKGeY_hNhrRZdqIg3g/viewform?usp=sf_link).

### Prerequisites

* Python 3.6
* Docker 18.09
* cincan-command

You are recommended to be running Ubuntu 18.04 or later. Apart from the base
Ubuntu system you need to have Docker installed (easiest: install the Ubuntu
`docker.io` package) and Python 3.6+

Install [`cincan-command`](https://gitlab.com/cincan/cincan-command) with the
respective instructions for your environment.

**NOTE**: New work-in-progress documentation for installing the tool in [here.](https://cincan.gitlab.io/cincan-command/installation.html)

Optional: You can also get a head-start by pulling and running the tools from
our [DockerHub](https://hub.docker.com/u/cincan) with
`cincan run cincan/[tool]` and trying them out.

## Workshop tasks

### APK malware -- Start at 8:30

[Link to the task](https://gitlab.com/CinCan/workshops/-/tree/master/Virtual%20Workshop/ApkTask)

Shady site has provided this shady APK as an drive-by download. It is believed
to be ransomware for Android phones. It encrypts files on the victim phone,
figure out some key facts about the ransomware.

Tools you can use in this task include:

 * apktool - APK reverse engineering
 * dex2jar - Android dex tools
 * jd-cmd - Java Decompiler
 * fernflower - Java decompiler
 * cfr - Java decompiler

**Join [WhereBy room](https://cincan.whereby.com/android) for support**

### Memory dump -- Start at 8:30

[Link to the task](https://gitlab.com/CinCan/workshops/-/tree/master/Virtual%20Workshop/MemoryDumpAnalysis)

This task is a bit like a CTF challenge, as there is a backstory behind the
memory dump. We are going to take a look at it with Volatility and extract
some pertinent artifacts

Tools used:

 * Volatility  - memory analysis
 * Some common Linux utilities

**Join [WhereBy room](https://cincan.whereby.com/memorydump) for support**

### Memory dump pt.2 -- Start at 9:30

[Link to the task](https://gitlab.com/CinCan/workshops/-/tree/master/Virtual%20Workshop/MemoryDumpAnalysis)

You can participate for this part even if you don't want to do memory analysis
part. We will give you a sample file.

This part is based on analysing a certain Word document which was extracted
from the memory. You can continue the analysis with these tools:

 * oledump - document analysis
 * oletools - suite of document analysis tools
 * ilspy - .NET Assembly based binary decompilation
 * radare2 - reverse engineering binaries
 * ghidra-decompiler - General machine-code decompilation
 * openssl and some common Linux utilities

**Join [WhereBy room](https://cincan.whereby.com/memorydump) for support**

### Phishing emails -- Start at 8:30

[Link to the task](https://gitlab.com/CinCan/workshops/-/tree/master/Virtual%20Workshop/PhishingTask)

Your organization has received some emails that might be malicious. You need
to analyze and figure out which are malicious and which are not.

Some of the tools you can use in this task:

 * scrape-website - scrape contents of URLs and screenshot the results
 * headless-thunderbird - screenshot .eml files with Thunderbird
 * oledump - document analysis
 * oletools - suite document analysis tools
 * ilspy - .NET Assembly based binary decompilation

**Join [WhereBy room](https://cincan.whereby.com/phishing) for support**
