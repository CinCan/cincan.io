disable_toc: true
title: Contact Us
---

<div class="bs-callout bs-callout-dark">
  <h4>Want to contact us?</h4>
  <p>
    Send email to: name-of-the-project@traficom.fi
  </p>
</div>

<div class="bs-callout row">
  <div class="column">
    <h4>Check out our GitLab</h4>
    <a href="https://gitlab.com/CinCan">
       <img class="img-fluid contact-img" src="/images/gitlab.png" />
    </a>
    <p> All source code is public</p>
    </div>
  <div class="column">
    <h4>Docker Hub</h4>
    <a href="https://hub.docker.com/u/cincan">
    <img class="img-fluid contact-img" src="/images/docker.jpg" />
    </a>
      <p>Newest containers are available on Docker Hub </p>
  </div>
  <div class="column">
    <h4>Twitter</h4>
    <a href="https://twitter.com/CinCanProject">
    <img class="img-fluid contact-img" src="/images/Twitter_Social_Icon_Circle_Color.png" />
    </a>
    <p>Check out our twitter</p>
  </div>
</div>