title: Pipelines

---

!!! note "Pipelines"

    Here is the list of Concourse CI pipelines created for the cincan project. The pipelines are downloadable [at GitLab](https://gitlab.com/CinCan/pipelines).

    You can setup the pilot environment and pipelines with the [instructions at our Gitlab page](https://gitlab.com/CinCan/environment).

<br>
### Descriptions  


| Pipeline name                                   | Tools used                                                     | Description  |
|-------------------------------------------------|----------------------------------------------------------------|--------------|
| [cuckoo-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/cuckoo-pipeline)              | Cuckoo sandbox                                                 | Cuckoo will provide a detailed report outlining the behavior of filea uploaded to Gitlab repository |
| [cortex_pipeline](https://gitlab.com/CinCan/pipelines/tree/master/cortex_pipeline)              | Cortex Abuse_Finder                         | Concourse pipeline that uses Cortex tool Abuse_Finder to analyze data such as IP, email or url |
| [document-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/document-pipeline)          | ClamAV/PDFiD/PeePDF/JSunpack-n/shellcode/strings/oledump/olevba| The pipeline clones the samples from a Gitlab repo, sorts files to PDF and other documents and then runs appropriate tools to the sample files. [Watch the VIDEO](https://gitlab.com/CinCan/vault/blob/master/screencast/document-pipeline.mp4)           |
| [email-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/email-pipeline)                | Honeynet Thug / Pywhois                                        |              |
| [MISP-integration](https://gitlab.com/CinCan/pipelines/tree/master/MISP-integration)            |                                                                | Example script that uses MISP zmq to listen for events with relevant attachments that could be further analysed with some CinCan pipeline. |
| [pdf-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/pdf-pipeline)                    | PDFiD / PeePDF / JSunpack-n / shellcode analysis.              | The pipeline polls for new files at a Gitlab repo, analyses the documents and writes the results to another branch of the repo. [Watch the VIDEO](https://gitlab.com/CinCan/vault/blob/master/screencast/pdf-pipeline.mp4)|
| [smart-factory-18-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/smart-factory-18-pipeline) | Strings / PE-scanner / Cortex                           | The pipeline tries to identify file type, then runs pe-scanner and strings, and finally Cortex results |
| [thug-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/thug-pipeline)                  | Honeynet Thug                                                  | Run a honeyclient (thug) on each URL in a file, get the analysis files in a separate commit |
| [virustotal-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/virustotal-pipeline)      | Suricata / iocextract / Virustotal                             | This pipeline consumes pcap files from s3 resource compatibible storage, archives pcaps and analyzes the files with suricata and virustotal. |
| [volatility-pipeline-1](https://gitlab.com/CinCan/pipelines/tree/master/volatility-pipeline-1)  | Volatility                                                     | Concourse pipeline that finds hidden processes and exports their executables to a git repo with Volatility. |


<br>
#### Pipelines currently with easy setup for the pilot environment:   


[pdf-pipeline](pdf-pipeline)  
[document-pipeline](document-pipeline)  


