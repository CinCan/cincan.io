disable_toc: true
title: Home

---


<div class="bs-callout bs-callout-dark">
  <h1> Let CinCan relieve your digital forensics pains </h1>
  <p>
    Run your analysis tools conveniently in containers using CinCan provided tooling. Save time, get rid of dependency hell, remove mistakes and keep your workflow robust.
  </p>
  <div>
    <a href="faq"> Read More ... </a>
    <br>
    <a href="img/overview-2020-02.pdf"> Or checkout our project overview slides (pdf)</a>
  </div>
</div>

<div class="bs-callout row">
  <div class="column">
    <a href="tools">
      <img src="img/tools.png" alt="Tools icon" class="marginauto"/>
    </a>
      <a href="tools">TOOLS</a>
      <p>Dockerized analysis tools</p>
    </div>
  <div class="column">
    <a href="https://gitlab.com/CinCan/cincan-command">
      <img src="img/cincan-command.png" class="marginauto" alt="Tools icon" />
      <a href="https://gitlab.com/CinCan/cincan-command">CINCAN-COMMAND</a>
      <p>Run native command-line tools provided as docker image </p>
    </a>
  </div>
  <div class="column">
    <a href="https://gitlab.com/CinCan/minion">
      <img src="img/minion.png" class="marginauto" alt="Tools icon" />
    </a>
    <a href="https://gitlab.com/CinCan/minion">MINION</a>
    <p> Build analysis pipelines using command-line tools and minion rules</p>
  </div>
</div>


  ![logo](img/logos/vol2.gif)


