# Cyberchess 2018 – an international cyber security conference

> Writers: Puuska Samir, Specialist & Ahonen Joni, Project worker, JAMK University of Applied Sciences


_**“Working alone is time wasted in applied research.”**_   
_–   Old Klingon proverb_

Annual international cybersecurity conference, the “Cyberchess 2018”, was held in Riga, Latvia on October 9th, 2018. The conference was held as a part of the European Cyber Security Month (ECSM) and we, the members of the CinCan project, participated conference with the aim to hear the latest news from the European cyber security field, a large set of talks, develop our project, and create new contacts with the Baltic area CERT operators.

The organisers of the conference, CERT.LV and ISACA, had built a focused program covering the latest trends of the cybersecurity field, such as 5G, IoT, and cryptocurrencies. We attended a diverse set of these talks, with the aspiration to find topics, news, and approaches that can be tied to part of a project development. Here is a bunch of topics captured from the talks we found interesting:

## ntop
[https://cert.lv/lv/2018/09/kiberdrosibas-konference-kibersahs-2018-runataji#Luca_Deri](https://cert.lv/lv/2018/09/kiberdrosibas-konference-kibersahs-2018-runataji#Luca_Deri)

Lecturer Luca Deri from the University of Pisa presented in his talk the tool named ntop that he has been developing since the 1998. The ntop is a high-performance network monitoring toolkit that offers various products focusing on the different parts of the network analysis:

- packet capturing
- traffic recording and replaying
- netflow, traffic analysis and enforcement
- deep packet inspection and Distributed Denial of Service (DDOS) mitigation and Virtual Private Network (VPN)

The product availability for the different platforms varies between the UNIX and Windows, but some of the products are only available on UNIX based platforms.

Since the amount of IoT devices has been growing rapidly in the last decade and the trend can be assumed to continue similar in near future, Deri aspired to demonstrate how the ntop can be used to monitor and capture network traffic related to the IoT devices and show how this captured information can be used to create correct firewall rules. This kind of actions are huge security improvement for example to the home network environments that contains nowadays several different IoT and other endpoint devices.

One of the targets of CinCan project is to offer a support for the network packet capturing samples so the integration of tools that can be used to network analysis is under research and including them to our Continuous Integration pipeline has started. You can read more about this interesting, mature project from its website: [https://www.ntop.org/](https://www.ntop.org/) or Github repository: [https://github.com/ntop](https://github.com/ntop).

## Malware sample re-weaponization
[https://cert.lv/lv/2018/09/kiberdrosibas-konference-kibersahs-2018-runataji#Karlis_Podins](https://cert.lv/lv/2018/09/kiberdrosibas-konference-kibersahs-2018-runataji#Karlis_Podins)

Want your own cyberweapon for free? in his talk Kārlis Podiņš (CERT.LV) explored the possibility for re-weaponizing advanced malware via binary editing. According to Podiņš, this allows persons with relatively modest experience to utilize advanced malware and benefit from the advanced exploitation techniques developed by more skilled teams. He demonstrated the feasibility of changing a Command & Control server’s IP address, and noted that even though obfuscation and other such methods may hinder the effort, it is still relatively easy to achieve.

This certainly will become the new normal, we’ve already seen hints of this during the infamous WannaCrypt pandemic. As many recalls, the original variant had a built in killswitch, first discovered by @MalwareTech. The ransomware tried to contact then unregistered URL and would only proceed if that failed to resolve. The URL was promptly registered, and the first wave was slowed down considerably. Unfortunately, soon researchers started to see variants where this check was disabled via quick binary edit. It’s also worth emphasizing that the WannaCry EternalBlue exploit was stolen from another malware.

Re-weaponized malware is also a method for evading attribution. Many threat actors can utilize similar looking infrastructure, as Podiņš pointed out. This is certainly a topic that must be addressed in the CinCan project. We can’t rely on purely mathematical similarity measures, such as ssdeep, for attribution. As metadata will play increasing role in malware analysis and classification, the analysis workflow and tools must support the analyst also in this area.

## Lessons from security incidents
[https://cert.lv/lv/2018/09/kiberdrosibas-konference-kibersahs-2018-runataji#Raymond_Schippers](https://cert.lv/lv/2018/09/kiberdrosibas-konference-kibersahs-2018-runataji#Raymond_Schippers)

Senior analyst Raymond Schippers from the Check Point Incident Response Team revealed in his talk the backgrounds from the four major incidents that they have been working with in the past few years. In addition, he told what kind of actions there were needed to mitigate these incidents.

Schippers pointed out that even if your organization has robust experience, latest technology and advanced IT staff, it not automatically guarantees that you are in safe from the security breaches. In fact, in most cases the targeted violation attempt does not even have to be highly advanced, since most of these incidents can happen because of the phishing campaign’s success. Schippers revealed in his talk that there have been cases where the attacker has used phished account privileges to gain local administrator privileges on the target host, because the organization’s Group Policy was not configured correctly. The costs in the cases like this can be highly extensive for the organization. In the worst scenario incident can lead the organization to the edge of bankruptcy.

The organisers will publish the previously presented topics and the other talks with their slides in near future with some constraints, so if you are interested read more about the event you can follow this website for the updates: [https://cert.lv/en/news](https://cert.lv/en/news).

## CinCan
The CinCan – Continuous integration for the Collaborative Analysis of Incidents is an EU funded project that has started at the beginning of the year 2018. The project constructs from different organizations and the JAMK University of Applied Sciences is one of the three project members. You can read more about the project and its goals from our [first blog post](/blog/2018_07_using_ci_to_automate).

> Original blog post: [https://blogit.jamk.fi/techtothefuture/2018/10/24/cyberchess-2018-an-international-cyber-security-conference/](https://blogit.jamk.fi/techtothefuture/2018/10/24/cyberchess-2018-an-international-cyber-security-conference/)