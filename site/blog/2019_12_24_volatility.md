# Day 24 - Volatility

> Writer: Ville Kalliokoski

_For the last entry in our advent calendar we have the powerhouse of static memory analysis - Volatility._

Would you like to see, what was going on inside a computer at some point in history? Memory dumps to the rescue! Memory dumps are, as the name suggests, everything from the volatile memory of a computer dumped in a file. This is a powerful tool for incident response and malware analysis alike, since you can go through everything from open processes to active and terminated connections and TCP/UDP sockets. Volatility is a collection of tools built to assist in researching these. Here we go over the basics of analyzing the processes found in the memory dump.

## Getting started with Volatility - analyzing processes

Volatility supports memory dumps for most operating systems, from Windows XP to OSX Sierra. Here we'll be using a dump from [Windows 7 SP1](https://gitlab.com/CinCan/tools/tree/master/_samples/memory). First thing we need is the correct memory profile for Volatility, so it knows what we are dealing with. Volatility offers couple of plugins for finding this information: `imageinfo`, which provides profile suggestions, and `kdbgscan`, which is designed to positively identify the correct profile (Note that the output of the commands below has been truncated).
`$ cincan run cincan/volatility kdbgscan -f _samples/memory/Win7SP1x86_2400.raw`
* `-f`: input path of the memory dump for volatility 
```
volatility: <= _samples/memory/Win7SP1x86_24000.raw
Volatility Foundation Volatility Framework 2.6.1
**************************************************
Instantiating KDBG using: /home/appuser/_samples/memory/Win7SP1x86_24000.raw WinXPSP2x86 (5.1.0 32bit)
Offset (P)                    : 0x2754b78
KDBG owner tag check          : True
Profile suggestion (KDBGHeader): Win7SP1x86_23418
Version64                     : 0x2754b50 (Major: 15, Minor: 7601)
PsActiveProcessHead           : 0x82769d70
PsLoadedModuleList            : 0x82771730
KernelBase                    : 0x8261c000

**************************************************
Instantiating KDBG using: /home/appuser/_samples/memory/Win7SP1x86_24000.raw WinXPSP2x86 (5.1.0 32bit)
Offset (P)                    : 0x2754b78
KDBG owner tag check          : True
Profile suggestion (KDBGHeader): Win7SP1x86_2400
Version64                     : 0x2754b50 (Major: 15, Minor: 7601)
PsActiveProcessHead           : 0x82769d70
PsLoadedModuleList            : 0x82771730
KernelBase                    : 0x8261c000
[snip]
```
From here we see that the first suggestion is `Win7SP1x86_23418`. In this case we know that this is not the correct one (see filename), and you can try this yourself with `psscan` and `pslist` in the next step: `pslist` works fine with the wrong profile, but `psscan` will not find any processes. 

From here we get to the fun part: diving in to the memory. One of the first steps you might try is look through the running processes in the system. Volatility offers multiple plugins for this as well: `pslist` shows the same listing you'd get from the Windows Task Manager, while `psscan` can also find hidden processes. `pstree` uses the same enumeration for processes as `pslist`, but shows the processes in tree form.

`$ cincan run cincan/volatility psscan --profile Win7SP1x86_24000 -f _samples/memory/Win7SP1x86_24000.raw`
* `--profile`: memory profile
```
volatility: <= _samples/memory/Win7SP1x86_24000.raw
Volatility Foundation Volatility Framework 2.6.1
Offset(P)          Name                PID   PPID PDB        Time created                   Time exited                   
------------------ ---------------- ------ ------ ---------- ------------------------------ ------------------------------
0x0000000001b22d20 sshd.exe           2036   1928 0x0f6e68c0 2019-11-21 09:37:06 UTC+0000                                 
0x0000000001ed53d0 SearchProtocol     2608   2532 0x0f6e6500 2019-11-21 09:37:12 UTC+0000                                 
0x00000000095b5d20 cygrunsrv.exe      1772    460 0x0f6e6740 2019-11-21 09:37:05 UTC+0000                                 
0x000000000ab73480 svchost.exe        2172    460 0x0f6e6900 2019-11-21 09:37:08 UTC+0000                                 
0x000000000b20dc40 dllhost.exe        2352    588 0x0f6e6940 2019-11-21 09:37:08 UTC+0000   2019-11-21 09:37:16 UTC+0000  
0x000000000b2a6888 cygrunsrv.exe      1928   1772 0x0f6e6800 2019-11-21 09:37:06 UTC+0000   2019-11-21 09:37:07 UTC+0000  
0x000000000b348d20 conhost.exe        1980    328 0x0f6e6840 2019-11-21 09:37:06 UTC+0000                                 
0x000000000b54d030 sppsvc.exe         2072    460 0x0f6e6300 2019-11-21 09:37:07 UTC+0000                                 
0x000000000ee13030 csrss.exe           364    356 0x0f6e6140 2019-11-21 18:36:56 UTC+0000                                 
0x000000000ee1d030 wininit.exe         372    320 0x0f6e6180 2019-11-21 18:36:56 UTC+0000                                 
[snip]                        
0x000000000f1ff088 csrss.exe           328    320 0x0f6e60c0 2019-11-21 18:36:55 UTC+0000                                 
0x000000000f6e5d20 smss.exe            252      4 0x0f6e6040 2019-11-21 18:36:51 UTC+0000                                 
0x000000000fc920f8 SearchIndexer.     2532    460 0x0f6e6780 2019-11-21 09:37:11 UTC+0000                                 
0x000000000fc94b98 VBoxTray.exe       1880   1508 0x0f6e67c0 2019-11-21 09:37:06 UTC+0000                                 
0x000000000fc98d20 SearchFilterHo     2644   2532 0x0f6e6980 2019-11-21 09:37:12 UTC+0000                                 
0x000000000fcb6688 services.exe        460    372 0x0f6e6100 2019-11-21 18:36:56 UTC+0000                                 
0x000000000ffed668 System                4      0 0x00185000 2019-11-21 18:36:51 UTC+0000
```
If you want to find out more about how the processes have been hidden, you can use `psxview`. It compares different sources of process listings and shows which source finds the processes.

`$ cincan run cincan/volatility psxview --profile Win7SP1x86_24000 -f _samples/memory/Win7SP1x86_24000.raw`
```
volatility: <= _samples/memory/Win7SP1x86_24000.raw
Volatility Foundation Volatility Framework 2.6.1
Offset(P)  Name                    PID pslist psscan thrdproc pspcid csrss session deskthrd ExitTime
---------- -------------------- ------ ------ ------ -------- ------ ----- ------- -------- --------
0x0ab73480 svchost.exe            2172 True   True   True     True   True  True    True     
0x0ef97440 svchost.exe            1396 True   True   True     True   True  True    True     
0x0b54d030 sppsvc.exe             2072 True   True   True     True   True  True    True     
0x0eebe188 svchost.exe             808 True   True   True     True   True  True    True     
0x0eeed310 audiodg.exe             988 True   True   True     True   True  True    True     
0x0fc94b98 VBoxTray.exe           1880 True   True   True     True   True  True    True     
0x0ef82918 spoolsv.exe            1348 True   True   True     True   True  True    True     
0x0eed6030 svchost.exe             904 True   True   True     True   True  True    True     
0x0eec99a0 svchost.exe             848 True   True   True     True   True  True    True     
[snip] 
0x0f6e5d20 smss.exe                252 True   True   True     True   False False   False    
0x0ee13030 csrss.exe               364 True   True   True     True   False True    True     
0x0b2a6888 cygrunsrv.exe          1928 True   True   False    True   False True    False    2019-11-21 09:37:07 UTC+0000
0x0ffed668 System                    4 True   True   True     True   False False   False    
0x0f1ff088 csrss.exe               328 True   True   True     True   False True    True     
0x0ef99740 userinit.exe           1404 False  True   False    False  False False   False    2019-11-21 09:37:32 UTC+0000
0x0b20dc40 dllhost.exe            2352 False  True   False    False  False False   False    2019-11-21 09:37:16 UTC+0000
```


If you want to look closer to any of the executables in processes, you can use `procdump` to dump the process executable to your disk:
`$ cincan run -d dump volatility procdump --profile=Win7SP1x86_24000 -D dump/ -p 2036 -f _samples/memory/Win7SP1x86_24000.raw`
* `-D`: path where the executable will be saved
* `-p`: process ID of the executable to be dumped
```
volatility: <= _samples/memory/Win7SP1x86_24000.raw
volatility: <= dump
volatility: => dump/executable.2036.exe
Volatility Foundation Volatility Framework 2.6.1
Process(V) ImageBase  Name                 Result
---------- ---------- -------------------- ------
0x84904d20 0x00400000 sshd.exe             OK: executable.2036.exe
```
(Note the `-d dump` flag for cincan command, see [Limitations for input/output](https://gitlab.com/CinCan/cincan-command) in the cincan command documentation.)

From [here](https://gitlab.com/CinCan/tools/tree/master/peframe) you can start [analyzing](https://gitlab.com/CinCan/tools/tree/master/binwalk) the [executable](https://gitlab.com/CinCan/tools/tree/master/snowman-decompile) or [dive](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference) even [deeper](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference-Mal) in to the memory dump.

## Other resources

[Volatility Foundation](https://www.volatilityfoundation.org/)

The CLI tool [cincan](https://gitlab.com/cincan/cincan-command) used here is available from PyPI, install it with `pip install cincan-command`. Installation of this tool is enough to get started -- the containers are downloaded automatically from either your local repository or DockerHub.

For more tools and sources take a look at [CinCan’s Gitlab repository](https://gitlab.com/CinCan/tools) and [Dockerhub](https://hub.docker.com/u/cincan/)!
