# Short presentation about CinCan & Advent calendar
> Writer: Karoliina Kemppainen

In the Cincan project we have recognised the most useful investigation tools by interviewing digital forensic professionals. A part of this project has been creation of docker images of those tools [https://hub.docker.com/u/cincan]. Currently we have dockerized over 60 different tools.  

This christmas we are going to present you the 24 most loved analysis tools selected by security specialists. Each day we will present new forensics tool and how it can be easily used with our command line tool called `cincan`. 

CinCan is INEA/CEF funded project worked on by NCSC-FI, JAMK University of Applied Sciences & University of Oulu. CinCan team aims to create semi automatic analysis pipelines and ease workflows of the digital forensics investigators.


### Follow us 
Advent calendar posts can be found from here (CinCan blog) and from Twitter https://twitter.com/CinCanProject


More information about this project can be found from https://cincan.io/ && https://gitlab.com/CinCan 

If you have anything to ask, please don’t hesitate to contact us: cincan@traficom.fi


# Cincan tool

Full instructions and source code can be found from
https://gitlab.com/CinCan/cincan-command

# CinCan command

The tool `cincan` command provide for a convenient use of the native command-line tools provided as docker images.

:warning: Currently the tool is a proof-of-concept under construction.

## Installation

As prerequisite you must have installed `Docker` for running the tools, and `Python 3` and `pip` Python package management program for the command program.
Consult your system documentation how to install them.

The command program is then installed using pip for Python 3:

    % sudo pip install cincan-command

If you invoke the pip installation with `sudo` the command `cincan` should be added to your path. Otherwise, you may need to do that yourself.

NOTE: You may want to install the tool into `virtualenv` to avoid conflicts with other Python applications you may have. Please consult appropriate documentation.

You can check that all works as follows:

    % cincan list

If all goes well you get a list of the tools dockerized in the 'Cincan' project. However, you can use any dockerized tools as long as they meet the
requirements listed in the end of this document. First time running this will take a while as it must fetch information of the tools
and cache it locally.

## Running tools with cincan

### Invoking tools

A tool can be invoked with cincan using 'run' sub-command like this:

    % cincan run <tool> <parameters..>

As you may remember you get the list of tools dockerized in 'CinCan' project
with `cincan list`.
For example the tool `cincan/pywhois`:

    % cincan run cincan/pywhois 127.0.0.1

Many tools give you help information, if you invoke them without arguments, for example:

    % cincan run cincan/tshark

More help is available with options like `-h` or `--help`, depending on the tool.

More detailed instructions and source code can be found from

https://gitlab.com/CinCan/cincan-command

*** 

Please feel welcome to follow us and our advent calendar project! 