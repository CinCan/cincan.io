# CinCan vs CovidLock

> Writer: Juha Kälkäinen and Jukka Pajukangas.

_Despite everyone being locked down due to COVID-19 the bad guys arent resting. Bunch of COVID-19 themed phising and malware are making rounds targeting everything from individuals to corporations. We at the CinCan project decided to take a look at one COVID-19 themed Android ransomware and see what we can dig up using tools we've dockerized._

### Covid-lock

Before we get started lets throw run this apk inside a sandbox and confirm that it indeed works as reported. The malware we're inspecting is called CovidLock (SHA256: c844992d3f4eecb5369533ff96d7de6a05b19fe5f5809ceb1546a3f801654890) and it was first discovered around early march. The sandbox environment we used was Android Oreo 8.1 with Nexus 5. Here is what we found out!

![pic1](../images/covid1.png)

The malware authors clearly could up their UI design skills. I guess Nexus 5 is not supported? 

Right off the bat the app requires special permissions to work such as screen lock and storage encryption. Without these the malware doesn’t seem to do anything. Since of course we wish to be alerted if a “coronavirus patient” is near you we give it all the rights it requests and wouldn’t you know it after a while your screen is greeted with the following ransom message.

![pic2](../images/covid2.png)

The link directs you to a Pastebin that gives you an address you can pay the requested Bitcoins (0.011B) to.
You can still somewhat use your phone and we could already see that no files had at least yet actually been encrypted. The malware does render your phone difficult to use since as soon as you try to open any application the ransom message appears. Reboot doesn’t help and removing the app without using safe mode, factory reset or ADB appears to be impossible. Alright, time to get our hands dirty, reverse the .apk and see what it actually contains using CinCan-command.

### Cincan-command

The purpose of CinCan command is to provide a convenient way to use native command line tools provided as docker images. All you need is some Linux distribution, Pip and Docker up and running. Entry level support for macOS is also available, but as always, things might break.

### Getting our hands dirty

CovidLock malware disguises itself as a corona information application, but in reality it locks the victims phone and shows ransom note demanding payment. Malware is thoroughly investigated by Tarik Saleh https://www.domaintools.com/resources/blog/covidlock-update-coronavirus-ransomware and here we demonstrate how the results could be achieved by using CinCan tools.

Reversing any unobfuscated .apk can be quite easy. You can install Cincan-command as follows:

```
sudo pip install cincan-command
```

Note that if you do not use sudo you’ll need to add cincan-command to path by hand.

[![asciicast](https://asciinema.org/a/w7HiynD1ZnYU5g6oIjBuXVQut.svg)](https://asciinema.org/a/w7HiynD1ZnYU5g6oIjBuXVQut)
 
By using Apktool with CinCan command we can retrieve the Android manifest file. 
You can decompile the .apk using apktool as follows:

```
cincan run cincan/apktool d CovidLock.apk
```

[![asciicast](https://asciinema.org/a/015qrI0uF9pt6MZs0oNo4orSO.svg)](https://asciinema.org/a/dkZYZI3bOqwDCtli5AhO2eSVI)

We can see that next permissions are specified in manifest file: FOREGROUND_SERVICE, RECEIVE_BOOT_COMPLETED, REQUEST_IGNORE_BATTERY_OPTIMIZATIONS and BIND_DEVICE_ADMIN BIND_ACCESSIBILITY_SERVICE.

By searching output of Apktool, we can find ransom note from resources at file /res/values/strings.xml

![pic3](../images/ransom.png)

We can use tools dex2jar and jadx easily with cincan-command to decompile the malware to Java source code files.
First lets decompile the .apk into a .jar file. Then we can use jadx to produce a more readable java source code.

```
cincan run cincan/dex2jar CovidLock.apk
cincan run cincan/jadx CovidLock.apk.jar
```

[![asciicast](https://asciinema.org/a/MTWjDJXNLGTU44hMJJ2BH3ER4.svg)](https://asciinema.org/a/MTWjDJXNLGTU44hMJJ2BH3ER4)

[![asciicast](https://asciinema.org/a/V4TJkORN2A4gXrch4cLKygXtC.svg)](https://asciinema.org/a/gQY4W2NHgQzYlrhXs3IYub7DW)

By inspecting decompiled Java source code, we can find correct unlocking code for the application.

![pic4](../images/password.png)

When we input the code “4865083501” into the malware it indeed terminates the application. You do however need to manually remove it or it locks your phone again after reboot.

Ransom note implies that the phone is encrypted and all data is erased or leaked if payment is not initiated in timeframe of 48 hours. Ransom note also claims that phone location is constantly tracked and “IF YOU TRY ANYTHING STUPID YOUR PHONE WILL BE AUTOMATICALLY ERASED”. Despite these claims, it seems that there is no actual functionality in the code to fulfill those threats. Actual encryption of data does not seem to happen, location of phone is not accessed and data transfer of personal files to remote location does not occur. If these observations are true, conclusion is that information stated in ransom note is simply empty threat to scare the victim into submission.

Ransom note is part of the application as shown earlier, but additional information to ransom note is specified behind separate URL.

![pic4](../images/link.png)

URL shown on image leads to another URL hxxps://pastebin.com/zg6rz6qT which shows next message:

![pic5](../images/pastebin.png)

Profitability of this malware does not seem to be very high as only 0.00018096 BTC ( ~1,17 €) has been transferred to that account so far.
It should also be noted that we found no evidence of any email or any other means of communication inside the malware. This likely means that even if you paid the authors they wouldn’t have any means of knowing who actually paid the ransom or to contact you with the code to remove the message.

All things considered while the malware functions as a succesful lock-screen attack it does not perform anything too sophisticated. 