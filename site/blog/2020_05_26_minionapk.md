# Automating your .apk reverse engineering workflow

Automate your malware analysis workflows with Minion!

Read how it was done for Android .apk files [here](https://medium.com/ouspg/automating-your-apk-reverse-engineering-workflow-95604e096402)