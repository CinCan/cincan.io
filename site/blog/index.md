title: Welcome to the blog

---

## Welcome to the CinCan blog

Here you can find all the project related blog posts. This is a great place to catch all the latest development updates and summaries.

Check out all the blog posts from the sidebar.

## News

!!! note "Virtual Workshop 11th of June!"
    Come experience CinCan tools at our online workshop on 2020-06-11! Kickoff at 08:00 UTC, DFIR exercises with chat support available until 11:00 - 12:00 with wrap-up & solution speedruns by CinCan project staff.

!!! note "Spring 2020"
    We are having a series of videos and blog post where we demonstrate usage of different analysis tools used with CinCan tools.

!!! note "Final Seminar"
    Final Seminar was held 12th of May 2020 via WhereBy.  

!!! note "Christmas 2019"
    Advent calendar starts at 1st of December. Presenting new tool each day.  

!!! note "April 2019"
    Presenting at [HelSec](https://helsec.fi) April 2019 meetup

!!! note "February 2019"
    Project hackathon / meetup is scheduled for 28.2. in Helsinki, along with first external users / open source participants.
    Launched project blog page.
    Presented at [TurkuSec](https://turkusec.fi/turkusec-february-meetup-2/) February meetup

!!! note "January 2019"
    Presented at 56th TF-CSIRT meeting & FIRST Regional Symposium Europe, in Tallinn, Estonia

!!! note "November 2018"
    CinCan project participates in Smart Factory 2018 fair. Event is held in Jyväskylä, Finland on 20-22 Nov 2018. You can find our stand C-408 from C1 hall where we are demonstrating how the project is aiming to improve CERT actors incident handling. Welcome!

