# Day 7 - Jsunpack-n

> Writer: Vesa Vertainen, Project Engineer, JAMK University of Applied Sciences

[Jsunpack-n](https://github.com/urule99/jsunpack-n) is one of the tools that has been dockerized in the [CinCan project](https://cincan.io). It was developed by Blake Hartstein, and it can be used to analyze URLs, as it emulates browser functionality, but also to unpack JavaScript, scan PDFs, and analyze PCAP. Let’s see some examples. First, let’s analyze a PDF with cincan/jsunpack-n, and output results to the current directory:

`$ docker run --rm -v $(data):/data cincan/jsunpack-n /data/testfile.pdf -V -d /data/results`

![pic1](../images/jsunpackn1.png)

Another, and maybe easier, way to run dockerized tools, is using the [cincan command line tool](https://gitlab.com/CinCan/cincan-command):

`$ cincan run cincan/jsunpack-n testfile.pdf -V -d results`

Jsunpack-n found nothing especially malicious, but it did find JavaScript, which was extracted to the results directory:

![pic2](../images/jsunpackn2.png)

To fetch information of a web site using the 'jsunpack' container with `cincan`, run:

`$ cincan run cincan/jsunpack-n -u "www.google.com"`

![pic3](../images/jsunpackn3.png)


This, and many other tools can be downloaded from the [CinCan’s Gitlab repository](https://gitlab.com/CinCan/tools), and the [Docker hub](https://hub.docker.com/u/cincan). You can also read more about Jsunpack-n and analyzing PDF files from our earlier blog post, [Analyzing malicious PDF documents using Dockerized tools](https://cincan.io/blog/2019_02_01_analyzing_pdf_documents/).
