# Day 17 - The Sleuth Kit

> Writer: Kimmo Linnavuo


_Next up in the CinCan Advent calendar we are sleuthing around hard disks and filesystems_

Today’s tool(s) are within [**The Sleuth Kit**](https://sleuthkit.org/sleuthkit/) which was created originally by **Brian Carrier**. Thanks for creating these handy tools!

The toolkit is a C library and a collection of tools to investigate filesystems inside disk images.

The Disk image formats that the tools support are raw disk images (produced with `dd`), `ewf` (Expert Witness, EnCase), `vmdk` (VirtualBox, VMWare) and `vhd` (Microsoft Virtual Hard Disk). The tools support filesystems such as `NTFS`, `FAT`, `ExFAT`, `UFS`, `ext2`, `ext3`, `ext4`, `HFS`, `ISO9660` and `YAFFS2`.

In this article we are focusing on a small raw test disk image with a `NTFS` filesystem. The image is the #7 test image taken from [Digital Forensics Tool Testing Images](http://dftt.sourceforge.net/test7/)

We will start with investigating the filesystem first with the `fsstat` tool using the cincan tool:

`cincan run cincan/sleuthkit fsstat _samples/disks/7-ntfs-undel.dd`

We should get this output and learn that it is indeed NTFS!:

```
cincan/sleuthkit: <= _samples/disks/7-ntfs-undel.dd
FILE SYSTEM INFORMATION
--------------------------------------------
File System Type: NTFS
Volume Serial Number: 285C576D5C5734B2
OEM Name: NTFS    
Volume Name: NTFS_DEL
Version: Windows XP

METADATA INFORMATION
--------------------------------------------
First Cluster of MFT: 2005
First Cluster of MFT Mirror: 4069
Size of MFT Entries: 1024 bytes
Size of Index Records: 4096 bytes
Range: 0 - 39
Root Directory: 5

CONTENT INFORMATION
--------------------------------------------
Sector Size: 512
Cluster Size: 1024
Total Cluster Range: 0 - 6015
Total Sector Range: 0 - 12031

...
```

We can then see what are the deleted files in the filesystem with the `fls` tool

```
cincan run cincan/sleuthkit fls -d ../_samples/disks/7-ntfs-undel.dd
```

We should get this output:

```
cincan/sleuthkit: <= _samples/disks/7-ntfs-undel.dd
-/r * 29-128-3: frag1.dat
-/r * 30-128-3: frag2.dat
-/r * 31-128-3: sing1.dat
-/r * 32-128-3: mult1.dat
-/r * 32-128-6: mult1.dat:ADS
-/d * 33-144-1: dir1
-/r * 37-128-1: res1.dat
```


Of course you can use this container without the cincan tool, here we can dump all deleted data of the filesystem with the `blkls` tool:

`docker run --rm -v $(pwd):/samples cincan/sleuthkit blkls /samples/disks/7-ntfs-undel.dd > deleted.dat`

You can then run additional tools on this data, such as `strings` or similar.

## Other resources

[Sleuthkit Informer](http://www.sleuthkit.org/informer/)

The CLI tool [cincan](https://gitlab.com/cincan/cincan-command) used here is available to run these different kind of dockerized tools. Installation of this tool is enough to get started -- the containers are downloaded automatically from either your local repository or DockerHub. You can install the `cincan` from PyPI with `pip install cincan-command`

Take a look for many other tools in [CinCan’s Gitlab repository!](https://gitlab.com/CinCan/tools)


