# Summer Hackathon: 3-days of Integrating CERT/CSIRT tools into automated incident response and malware analysis pipelines.

> Writer: Juha Kälkäinen

Computer Incident Response Center Luxembourg organized the fourth Open Source Security Software Hackathon on August 7–9 2018. The 3-day hackathon event took place in Luxembourg with the goal to improve interoperability and integration between different open source tools for incident response malware analysis. This presented us working with the CinCan project a great opportunity to meet new people, present our project, get feedback, and integrate new tools into our automated incident response and malware analysis pipeline framework.

We met a bunch of infosec pros working on different projects at the event and got some hands on experience with some of the tools that they were working on. We succesfully integrated a couple of them into our existing CI pipeline framework and got some good ideas on what to work on after the event.

Here are few of the tools that we took a closer look at the event;

![misp](/images/misp.png)

_[https://github.com/MISP/MISP](https://github.com/MISP/MISP)_

**MISP** (or **M**alware **I**nformation **S**haring **P**latform and **T**hreat **S**haring) was one of the main focuses for development at the hackathon. It is described on MISP Github as follows: _“MISP, is an open source software solution for collecting, storing, distributing and sharing cyber security indicators and threat about cyber security incidents analysis and malware analysis. MISP is designed by and for incident analysts, security and ICT professionals or malware reverser to support their day-to-day operations to share structured informations efficiently.”_

![circl](/images/circl.png)

_[https://github.com/CIRCL/AIL-framework](https://github.com/CIRCL/AIL-framework)_

**AIL** is a modular framework to analyse unstructured data sources for potential information leaks from sources such as Pastebin or Github Gist. The framework is being developed mainly by Computer Incident Response Center Luxembourg and offers some functionality to work together with MISP. Two developers from the project were present at the hackathon on the first day and they were eager to show us some hands-on stuff on how the framework functions.

![viper](/images/viper.png)

[https://github.com/viper-framework/viper](https://github.com/viper-framework/viper)

**Viper** is a binary analysis and management framework. It aims to provide a solution for easy organization of malware and scripts created for analysis. The developer aptly describes Viper as follows: _“Metasploit for malware researchers: it provides a terminal interface that you can use to store, search and analyze arbitraty files with and a framework to easily create plugins of any sort.”_

![manalyze](/images/manalyze.png)

[https://github.com/JusticeRage/Manalyze](https://github.com/JusticeRage/Manalyze)

**Manalyze** is a static analyser for PE files with a flexible plugin architecture. It offers a robust parser which allows users to statically analyze PE files in-depth.

## Our work on AIL Pipeline

![pipelinegif](/images/pipeline.gif)

_[https://gitlab.com/CinCan/Tools/tree/master/pipelines/AIL_pipe](https://gitlab.com/CinCan/Tools/tree/master/pipelines/AIL_pipe)_

After meeting with different developers present at the hackathon and taking a closer look at their projects we first decided to implement an automated pipeline to analyse data dumps by using AIL. The pipeline works much like the other prototypes we’ve built before on the project. It uses a Git repository as a workspace for scripts that are used by the pipeline and for file download/upload. When the pipeline is set up correctly a user can just upload any data dump(s) to the Git repository and the pipeline will automatically analyse the files using AIL.

## MISP-AIL-pipeline

![ail-pipeline](/images/ail-pipeline.png)

_[https://gitlab.com/CinCan/Tools/tree/master/pipelines/AIL-MISP-pipeline](https://gitlab.com/CinCan/Tools/tree/master/pipelines/AIL-MISP-pipeline)_

The second pipeline we built during the hackathon was the MISP-AIL-pipeline. As the name suggests it uses both MISP and AIL projects in a single pipeline. The goal for MISP-AIL pipeline was to test how well we can integrate these two projects to work together using Concourse. The first job for the pipeline is to fetch a data dump from an MISP entry and upload it to Git resource-repo. If the job succeeds job-ail-analyze starts which creates a new entry to AIL using the MISP data dump as an input, waits for AIL to analyse the data and then attempts to fetch any tags AIL gave it. Any results are written to a file which is then uploaded to resource-repo. This pipeline works as an example prototype on how we can automate analysis of different MISP entries.

Want to know more about the project or try our other prototypes? See the project Gitlab page: [https://gitlab.com/CinCan](https://gitlab.com/CinCan). For a more detailed description of the CinCan project see our [previous blog post](/blog/2018_10_01_summer_hackathon).

> Original blog post: [https://medium.com/ouspg/summer-hackathon-3-days-of-integrating-cert-csirt-tools-into-automated-incident-response-and-1cba6defd0e0](https://medium.com/ouspg/summer-hackathon-3-days-of-integrating-cert-csirt-tools-into-automated-incident-response-and-1cba6defd0e0)