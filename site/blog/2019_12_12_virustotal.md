# Day 12 - Virustotal
> Writer: Juha Kälkäinen - OUSPG / University of Oulu

We at the CinCan project have written and dockerized a script that can be used to query the [Virustotal](https://www.virustotal.com) service database.

Say you have a list of potential malicious URLs and you wish to find out what Virustotal knows about them. Copy-pasting them by hand one-by-one takes a while, especially if there's a lot of them. Here is where the virustotal tool comes in handy! Using the Virustotal API it automatically sends each URL or hash from a text file to Virustotal and outputs the results to a folder of your choosing. Only have the free API key with limited access? No worries! The tool automatically attempts to buffer the requests once your limit of 4 queries per minute has been reached.  

This is how you can use virustotal API with cincan. Let's use a sample file containing the url https://www.virustotal.com as an example. To query what Virustotal knows about itself you can just run the following command inside the samples folder:
```
cincan run cincan/virustotal --url_file url_example.txt -o output/ --api_key supersecretapikey  
```

The output should produce familiar .json and .log reports that should look something like this:
```
{
  "https://www.virustotal.com": {
    "scan_id": "f1177df4692356280844e1d5af67cc4a9eccecf77aa61c229d483b7082c70a8e-1575458687",
    "resource": "https://www.virustotal.com",
    "url": "https://www.virustotal.com/",
    "response_code": 1,
    "scan_date": "2019-12-04 11:24:47",
    "permalink": "https://www.virustotal.com/url/f1177df4692356280844e1d5af67cc4a9eccecf77aa61c229d483b7082c70a8e/analysis/1575458687/",
    "verbose_msg": "Scan finished, scan information embedded in this object",
    "filescan_id": null,
    "positives": 0,
    "total": 72,
    "scans": {
      "CLEAN MX": {
        "detected": false,
        "result": "clean site
...

```

This, and many other tools can be downloaded from the [CinCan’s Gitlab repository](https://gitlab.com/CinCan/tools), and the [Docker hub](https://hub.docker.com/u/cincan).

