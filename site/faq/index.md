# FAQ

### Question: What is this project  about?

### Answer
This project has produced following achievements
#### 1) Identifying important open source DFIR tools and packaging them in reproducible containers
By using questionnaires and interviews we have recognised multiple open source digital forensics incident response tools which are widely used and trusted among incident response professionals. We have packed and contained these tools with docker aiming for reproducibility and integrity. Check all the tools from our [tools repository](https://gitlab.com/CinCan/tools) or from [dockerhub](https://hub.docker.com/u/cincan)

#### 2) Capturing and automating DFIR workflows through tool pipelines
We have tried to recognise workflows of digital forensics and create tools and pipelines to automate common actions of dfir analysts. Check our approach for this by reading more about our tools and how to use them

 - [cincan-command](https://gitlab.com/CinCan/cincan-command) 
 - [minion](https://gitlab.com/CinCan/minion)

#### 3) Piloting with incident responders and digital forensics experts
We have piloted our tools and workflow approaches with multiple professionals, gathered feedback and tried to iterate our work based on this feedback. We are actively looking for more pilot customers. If interested to hear more, please contact us!


### Minor research areas 

#### 4) Quality of threat intelligence
Map the state of the art of threat intelligence feed providers, feeds, contents of the feeds, and possible evaluation sources for feed attributes throughout the project. Map the state of the art of threat intelligence feed providers, feeds, contents of the
    feeds, and possible evaluation sources for feed attributes throughout the project. 


### Question: Where does the name CinCan comes from? 
### Answer
It means Continuous Integration for the Collaborative Analysis of Incidents. 
